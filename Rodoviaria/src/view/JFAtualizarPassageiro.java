package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;

public class JFAtualizarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtGenero;
	private JTextField txtRG;
	private JTextField txtCPF;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtTelefone;
	private static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAtualizarPassageiro frame = new JFAtualizarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param id 
	 */
	public JFAtualizarPassageiro(int id) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 469, 494);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PassageiroDAO pdao = new PassageiroDAO();
		Passageiro p = pdao.read(id);
		
		JLabel lblidP = new JLabel("ID Passageiro:");
		lblidP.setBounds(200, 11, 117, 14);
		contentPane.add(lblidP);
		
		JLabel lblID = new JLabel("New label");
		lblID.setBounds(350, 11, 46, 14);
		contentPane.add(lblID);
		
		JLabel lblAlterarPassageiro = new JLabel("Alterar Passageiro");
		lblAlterarPassageiro.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAlterarPassageiro.setBounds(10, 11, 414, 23);
		contentPane.add(lblAlterarPassageiro);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(10, 45, 86, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 60, 414, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblGenero = new JLabel("G\u00EAnero");
		lblGenero.setBounds(10, 91, 46, 14);
		contentPane.add(lblGenero);
		
		txtGenero = new JTextField();
		txtGenero.setBounds(10, 107, 414, 20);
		contentPane.add(txtGenero);
		txtGenero.setColumns(10);
		
		JLabel lblRG = new JLabel("RG");
		lblRG.setBounds(10, 138, 414, 14);
		contentPane.add(lblRG);
		
		txtRG = new JTextField();
		txtRG.setBounds(10, 154, 414, 20);
		contentPane.add(txtRG);
		txtRG.setColumns(10);
		
		JLabel lblCPF = new JLabel("CPF");
		lblCPF.setBounds(10, 185, 414, 14);
		contentPane.add(lblCPF);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(10, 199, 414, 20);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o");
		lblEndereco.setBounds(10, 230, 414, 14);
		contentPane.add(lblEndereco);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(10, 244, 414, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setBounds(10, 275, 414, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(10, 289, 414, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(10, 320, 414, 14);
		contentPane.add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(10, 345, 414, 20);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		lblID.setText(String.valueOf(p.getIdPassageiro()));
		txtNome.setText(p.getNome());
		txtGenero.setText(p.getGenero());
		txtRG.setText(String.valueOf(p.getRg()));
		txtCPF.setText(p.getCpf());;
		txtEndereco.setText(p.getEndereco());
		txtEmail.setText(p.getEmail());
		txtTelefone.setText(String.valueOf(p.getTelefone()));
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				p.setIdPassageiro(Integer.parseInt(lblID.getText()));
				p.setNome(txtNome.getText());
				p.setGenero(txtGenero.getText());
				p.setRg(Long.parseLong(txtRG.getText()));
				p.setCpf(txtCPF.getText());
				p.setEndereco(txtEndereco.getText());
				p.setEmail(txtEmail.getText());
				p.setTelefone(Long.parseLong(txtTelefone.getText()));		
				
				dao.update(p);
				dispose();
			}
		});
		btnAlterar.setBounds(10, 389, 111, 23);
		contentPane.add(btnAlterar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
				txtGenero.setText(null);
				txtRG.setText(null);
				txtCPF.setText(null);
				txtEndereco.setText(null);
				txtEmail.setText(null);
				txtTelefone.setText(null);
			}
		});
		btnLimpar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnLimpar.setBounds(154, 389, 95, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnCancelar.setBounds(285, 389, 95, 23);
		contentPane.add(btnCancelar);

	}
}
